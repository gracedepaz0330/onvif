package com.psi.nusa.gateway.configurations;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class ApplicationInitializer 
  implements WebApplicationInitializer {
  
    @Override
    public void onStartup(ServletContext servletContext) 
      throws ServletException {
  
        AnnotationConfigWebApplicationContext context 
          = new AnnotationConfigWebApplicationContext();
  
        servletContext.addListener(new ContextLoaderListener(context));
        servletContext.setInitParameter(
          "contextConfigLocation", "com.psi.nusa");
    }
}