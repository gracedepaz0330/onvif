package com.psi.nusa.gateway.constants;

public class OnvifConstants {
	
	public static String DEFAULT_ONVIF_DEVICE_SUFFIX = "/onvif/device_service";
	public static String HTTP_PROTOCOL = "http://";
	public static String RTSP_PROTOCOL = "rtsp://";
	
	public static String PATH_PROJ_CONFIG = "project.config.path";
	public static final String PATH_CATALINA_HOME = "catalina.home";
	public static final String PATH_CATALINA_BASE = "catalina.base";
	public static final String PROJ_JBOSS_HOME = "jboss.server.config.dir";
	
	public static final String CATCH_ALL_CODE = "9999";
	public static final String CATCH_ALL_DESC = "Unknown Error Occured";
	public static final String SUCCESS_CODE = "0000";
	public static final String SUCCESS_DESCRIPTION = "Success";

}
