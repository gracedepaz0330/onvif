package com.psi.nusa.gateway.controllers;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.onvif.ver10.schema.Capabilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.psi.nusa.gateway.models.OnvifDevice;
import com.psi.nusa.gateway.services.DeviceService;
import com.psi.nusa.gateway.utils.OnvifUtil;

@Path("/device")
public class OnvifDeviceController extends SpringBeanAutowiringSupport {
	
	@Autowired
	@Qualifier("DeviceService")
	DeviceService deviceService;
	
	@PostConstruct
    public void init() {
        processInjectionBasedOnCurrentContext( SpringBeanAutowiringSupport.class );
    }
	
	@GET
	@Path("/getCapabilities")
	public String getCapabilities(@QueryParam("ip") String ip, @QueryParam("username") String username,
			@QueryParam("password") String password) {
		
		OnvifDevice onvifDevice = new OnvifDevice(ip, "", username, password, "", "");
		Capabilities capabilities = deviceService.getCapabilities(onvifDevice);
		
		return OnvifUtil.getStringValue(capabilities);
		
	}

}
