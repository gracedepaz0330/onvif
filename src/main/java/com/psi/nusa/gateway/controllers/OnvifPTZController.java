package com.psi.nusa.gateway.controllers;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.onvif.ver10.schema.PTZPreset;
import org.onvif.ver20.ptz.wsdl.GetConfigurationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.psi.nusa.gateway.models.OnvifDevice;
import com.psi.nusa.gateway.models.PTZResponse;
import com.psi.nusa.gateway.services.PTZService;
import com.psi.nusa.gateway.utils.OnvifUtil;

@Path("/ptz")
public class OnvifPTZController extends SpringBeanAutowiringSupport {
	
	@Autowired
	@Qualifier("PTZService")
	PTZService ptzService;
	
	@PostConstruct
    public void init() {
        processInjectionBasedOnCurrentContext( SpringBeanAutowiringSupport.class );
    }
	
	@GET
	@Path("/getPTZConfiguration")
	@Produces("application/json")
	public String getPTZConfiguration(@QueryParam("ip") String ip, @QueryParam("username") String username,
			@QueryParam("password") String password, @QueryParam("ptzToken") String ptzToken) {
		OnvifDevice onvifDevice = new OnvifDevice(ip, "", username, password, "", "");
		GetConfigurationResponse ptzConfig = ptzService.getPTZConfig(onvifDevice);
		
		return OnvifUtil.getStringValue(ptzConfig);
		
	}
	
	@GET
	@Path("/doAbsoluteMove")
	@Produces("application/json")
	public String doAbsoluteMove(@QueryParam("ip") String ip, @QueryParam("username") String username,
			@QueryParam("password") String password, @QueryParam("profileToken") String profileToken,
			@QueryParam("ptzToken") String ptzToken,
			@QueryParam("x") float x, @QueryParam("y") float y, @QueryParam("z") float z) {
		OnvifDevice onvifDevice = new OnvifDevice(ip, "", username, password, "", "");
		onvifDevice.setActiveProfileToken(profileToken);
		onvifDevice.setActivePtzToken(ptzToken);
		onvifDevice.setxValue(x);
		onvifDevice.setyValue(y);
		onvifDevice.setzValue(z);
		PTZResponse ptzResponse = ptzService.doAbsoluteMove(onvifDevice);
		
		return OnvifUtil.getStringValue(ptzResponse);
		
	}
	
	@GET
	@Path("/doRelativeMove")
	@Produces("application/json")
	public String doRelativeMove(@QueryParam("ip") String ip, @QueryParam("username") String username,
			@QueryParam("password") String password, @QueryParam("profileToken") String profileToken,
			@QueryParam("ptzToken") String ptzToken,
			@QueryParam("x") float x, @QueryParam("y") float y, @QueryParam("z") float z) {
		OnvifDevice onvifDevice = new OnvifDevice(ip, "", username, password, "", "");
		onvifDevice.setActiveProfileToken(profileToken);
		onvifDevice.setActivePtzToken(ptzToken);
		onvifDevice.setxValue(x);
		onvifDevice.setyValue(y);
		onvifDevice.setzValue(z);
		PTZResponse ptzResponse = ptzService.doRelativeMove(onvifDevice);
		
		return OnvifUtil.getStringValue(ptzResponse);
		
	}
	
	@GET
	@Path("/getPresets")
	@Produces("application/json")
	public String getPresets(@QueryParam("ip") String ip, @QueryParam("username") String username,
			@QueryParam("password") String password, @QueryParam("profileToken") String profileToken) {
		OnvifDevice onvifDevice = new OnvifDevice(ip, "", username, password, "", "");
		onvifDevice.setActiveProfileToken(profileToken);
		List<PTZPreset> presets = ptzService.getPresets(onvifDevice);	
		
		return OnvifUtil.getStringValue(presets);
	}
	
	@GET
	@Path("/goToPreset")
	@Produces("application/json")
	public String goToPreset(@QueryParam("ip") String ip, @QueryParam("username") String username,
			@QueryParam("password") String password, @QueryParam("profileToken") String profileToken,
			@QueryParam("presetToken") String presetToken) {
		OnvifDevice onvifDevice = new OnvifDevice(ip, "", username, password, "", "");
		onvifDevice.setActiveProfileToken(profileToken);
		onvifDevice.setPresetToken(presetToken);
		PTZResponse ptzResponse = ptzService.goToPreset(onvifDevice);
		
		return OnvifUtil.getStringValue(ptzResponse);
		
	}

}
