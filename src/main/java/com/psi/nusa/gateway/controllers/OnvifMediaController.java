package com.psi.nusa.gateway.controllers;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.onvif.ver10.schema.MediaUri;
import org.onvif.ver10.schema.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.psi.nusa.gateway.models.OnvifDevice;
import com.psi.nusa.gateway.services.MediaService;
import com.psi.nusa.gateway.utils.OnvifUtil;


@Path("/media")
public class OnvifMediaController extends SpringBeanAutowiringSupport {
	
	@PostConstruct
    public void init() {
        processInjectionBasedOnCurrentContext( SpringBeanAutowiringSupport.class );
    }
	
	@Autowired
	@Qualifier("MediaService")
	MediaService mediaService;
	
	
	@GET
	@Path("/verify")
	@Produces("text/html")
	public Response getStartingPage() 
	{
		String output = "<h1> Gateway is working! <h1>" +
				"<p>Yaaay! It's working ... <br>Ping @ " + new Date().toString() + "</p> <br>";
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/getProfiles")
	@Produces("application/json")
	public String getProfiles(@QueryParam("ip") String ip, @QueryParam("username") String username,
			@QueryParam("password") String password) {
		OnvifDevice onvifDevice = new OnvifDevice(ip, "", username, password, "", "");
		List<Profile> profiles = mediaService.getProfiles(onvifDevice);
		
		return  OnvifUtil.getStringValue(profiles);
		
	}
	
	@GET
	@Path("/getProfileByToken")
	@Produces("application/json")
	public String getProfileByToken(@QueryParam("ip") String ip, @QueryParam("username") String username,
			@QueryParam("password") String password, @QueryParam("profileToken") String profileToken) {
		OnvifDevice onvifDevice = new OnvifDevice(ip, "", username, password, "", "");
		onvifDevice.setActiveProfileToken(profileToken);
		Profile profile = mediaService.getProfileByToken(onvifDevice);
		
		return  OnvifUtil.getStringValue(profile);
		
	}
	
	
	@GET
	@Path("/getStreamUri")
	@Produces("application/json")
	public String getStreamUri(@QueryParam("ip") String ip, @QueryParam("username") String username,
			@QueryParam("password") String password, @QueryParam("profileToken") String profileToken,
			@QueryParam("protocol") String protocol, @QueryParam("stream") String stream) {
		OnvifDevice onvifDevice = new OnvifDevice(ip, "", username, password, "", "");
		onvifDevice.setActiveProfileToken(profileToken);
		onvifDevice.setProtocol(protocol);
		onvifDevice.setStream(stream);
		MediaUri mediaUri = mediaService.getStreamUri(onvifDevice);
		return OnvifUtil.getStringValue(mediaUri);
	}
	
	@POST
	@Path("/alarm")
	public String alarm(String message) {
		System.out.println(message);
		return "alarm";
	}

}
