package com.psi.nusa.gateway.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;

public class SecurityUtil {
	
	public static String encryptPassword(String password, String nonce, String utcTime) {
		String beforeEncryption = nonce + utcTime + password;
        byte[] encryptedRaw;
        
        try {
            encryptedRaw = sha1(beforeEncryption);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        return Base64.encodeBase64String(encryptedRaw);
	}
	
	public static String createNonce() {
        Random generator = new Random();
        return "" + generator.nextInt();
    }
	
	public static String encryptNonce(String nonce) {
        return Base64.encodeBase64String(nonce.getBytes());
    }
	
	public static byte[] sha1(String s) throws NoSuchAlgorithmException {
        MessageDigest SHA1 = null;
        SHA1 = MessageDigest.getInstance("SHA1");

        SHA1.reset();
        SHA1.update(s.getBytes());

        return SHA1.digest();
    }


}
