package com.psi.nusa.gateway.utils;

import java.io.File;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.psi.nusa.gateway.constants.OnvifConstants;

public class OnvifUtil {
	
	private static ObjectMapper mapper = new ObjectMapper();
	
	public static String getStringValue(Object object) {
		try {
			return OnvifUtil.mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static String getPropertyPath() {
		// Get from PROJECT CONFIGURATION server
		String propertyHome = System.getProperty(OnvifConstants.PATH_PROJ_CONFIG) != null
				? System.getProperty(OnvifConstants.PATH_PROJ_CONFIG) : System.getenv(OnvifConstants.PATH_PROJ_CONFIG);

		// Get from TOMCAT server
		if (propertyHome == null) {
			propertyHome = System.getProperty(OnvifConstants.PATH_CATALINA_HOME) != null
					? System.getProperty(OnvifConstants.PATH_CATALINA_HOME)
					: System.getProperty(OnvifConstants.PATH_CATALINA_BASE);
			if (propertyHome != null) {
				propertyHome = propertyHome + File.separator + "conf";
			}
		}

		// Get from JBOSS server
		if (propertyHome == null) {
			propertyHome = System.getProperty(OnvifConstants.PROJ_JBOSS_HOME);
			if (propertyHome != null) {
				propertyHome = propertyHome + File.separator + "configuration";
			}
		}

		
		return propertyHome;
	}

}
