package com.psi.nusa.gateway.services;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.springframework.beans.factory.annotation.Autowired;

import com.psi.nusa.gateway.soap.SoapFactory;

/**
 * 
 * @author kul_grace
 *
 */
public abstract class OnvifService {
	
	@Autowired
	SoapFactory soapFactory;
	
	/**
	 * Execute SOAP Call
	 * 
	 * @param requestElement
	 * @param uri
	 * @param username
	 * @param password
	 * @param authenticate
	 * @return
	 */
	public SOAPMessage executeSOAPCall(Object requestElement, String uri, String username, String password,
			boolean authenticate) {
		
		SOAPMessage soapRequest = null;
		SOAPMessage soapResponse = null;
		SOAPConnection soapConnection = null;
		
		try {
			soapConnection = soapFactory.establishSoapConnection();
			soapRequest = soapFactory.createSOAPMessage(requestElement);
			if (authenticate) {
				soapFactory.createSoapHeader(soapRequest, username, password);
			}
			if(soapRequest != null) {
				soapResponse = soapConnection.call(soapRequest, uri);
			}
		} catch (UnsupportedOperationException | SOAPException e) {
			e.printStackTrace();
		} finally {
			if(soapConnection != null) {
				try {
					soapConnection.close();
				} catch (SOAPException e) {
					e.printStackTrace();
				}
			}
		}
		
		return soapResponse;
	}
	
	/**
	 * Convert soap response to desired object
	 * 
	 * @param soapResponse
	 * @param responseElement
	 * @return
	 */
	public Object getResponseElement(SOAPMessage soapResponse, Object responseElement) {
		Unmarshaller unmarshaller;
		try {
			unmarshaller = JAXBContext.newInstance(responseElement.getClass()).createUnmarshaller();
			return unmarshaller.unmarshal(soapResponse.getSOAPBody().extractContentAsDocument());
		} catch (JAXBException | SOAPException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
