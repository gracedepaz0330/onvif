package com.psi.nusa.gateway.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.onvif.ver10.schema.Capabilities;
import org.onvif.ver10.schema.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.psi.nusa.gateway.models.DeviceConfiguration;
import com.psi.nusa.gateway.models.OnvifDevice;
import com.psi.nusa.gateway.utils.OnvifUtil;

@Service
public class ConfigurationService {
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	MediaService mediaService;

	public synchronized void saveConfiguration(DeviceConfiguration config) {
		BufferedWriter bw = null;
		FileWriter fw = null;

		String content = OnvifUtil.getStringValue(config);
		String filename = config.getOnvifDevice().getIp() + ".txt";
		try {
			fw = new FileWriter(OnvifUtil.getPropertyPath() + "/" + filename);
			bw = new BufferedWriter(fw);
			bw.write(content);
			System.out.println("Config File for " + config.getOnvifDevice().getIp() + 
					"is successsfully saved");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null) {
					bw.close();
				}
				if (fw != null) {
					fw.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public DeviceConfiguration formConfigurtionContent(OnvifDevice onvifDevice) {
		DeviceConfiguration deviceConfig = new DeviceConfiguration();
		
		Capabilities capabilities = deviceService.getCapabilities(onvifDevice);
		List<Profile> profiles = mediaService.getProfiles(onvifDevice);
		
		deviceConfig.setOnvifDevice(onvifDevice);
		deviceConfig.setCapabilities(capabilities);
		deviceConfig.setProfiles(profiles);
		
		return deviceConfig;
	}
	
	public boolean isConfigurationExist(String filename) {
		File file = new File(filename);
		return file.exists();
	}
}
