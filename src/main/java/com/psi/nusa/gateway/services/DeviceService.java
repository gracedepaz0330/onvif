package com.psi.nusa.gateway.services;

import javax.xml.soap.SOAPMessage;

import org.onvif.ver10.device.wsdl.GetCapabilities;
import org.onvif.ver10.device.wsdl.GetCapabilitiesResponse;
import org.onvif.ver10.schema.Capabilities;
import org.onvif.ver10.schema.CapabilityCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.psi.nusa.gateway.constants.OnvifConstants;
import com.psi.nusa.gateway.models.OnvifDevice;
import com.psi.nusa.gateway.soap.OnvifSoapFactory;

/**
 * 
 * @author kul_grace
 *
 */
@Service
@Qualifier("DeviceService")
public class DeviceService extends OnvifService {
	
	@Autowired
	@Qualifier("OnvifSoapFactory")
	OnvifSoapFactory soapFactory;
	
	/**
	 * 
	 * @param onvifDevice
	 * @return
	 */
	public Capabilities getCapabilities(OnvifDevice onvifDevice) {
		GetCapabilities capabilitiesRequest = new GetCapabilities();
		GetCapabilitiesResponse capabilitiesResponse = new GetCapabilitiesResponse();
		String uri = OnvifConstants.HTTP_PROTOCOL + onvifDevice.getIp() + OnvifConstants.DEFAULT_ONVIF_DEVICE_SUFFIX;
		
		CapabilityCategory category = CapabilityCategory.ALL;
		capabilitiesRequest.getCategory().add(category);
		
		try {
			SOAPMessage soapResponse = executeSOAPCall(capabilitiesRequest, uri, 
					onvifDevice.getUsername(), onvifDevice.getPassword(), true);
			
			capabilitiesResponse = (GetCapabilitiesResponse) getResponseElement(soapResponse, capabilitiesResponse);
			return capabilitiesResponse.getCapabilities();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
