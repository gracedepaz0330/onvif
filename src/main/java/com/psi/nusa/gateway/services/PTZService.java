package com.psi.nusa.gateway.services;

import java.util.List;

import javax.xml.soap.SOAPMessage;

import org.onvif.ver10.schema.PTZConfiguration;
import org.onvif.ver10.schema.PTZPreset;
import org.onvif.ver10.schema.PTZVector;
import org.onvif.ver10.schema.Vector1D;
import org.onvif.ver10.schema.Vector2D;
import org.onvif.ver20.ptz.wsdl.AbsoluteMove;
import org.onvif.ver20.ptz.wsdl.AbsoluteMoveResponse;
import org.onvif.ver20.ptz.wsdl.GetConfiguration;
import org.onvif.ver20.ptz.wsdl.GetConfigurationResponse;
import org.onvif.ver20.ptz.wsdl.GetConfigurations;
import org.onvif.ver20.ptz.wsdl.GetConfigurationsResponse;
import org.onvif.ver20.ptz.wsdl.GetPresets;
import org.onvif.ver20.ptz.wsdl.GetPresetsResponse;
import org.onvif.ver20.ptz.wsdl.GotoPreset;
import org.onvif.ver20.ptz.wsdl.GotoPresetResponse;
import org.onvif.ver20.ptz.wsdl.RelativeMove;
import org.onvif.ver20.ptz.wsdl.RelativeMoveResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.psi.nusa.gateway.constants.OnvifConstants;
import com.psi.nusa.gateway.models.OnvifDevice;
import com.psi.nusa.gateway.models.PTZResponse;
import com.psi.nusa.gateway.soap.OnvifSoapFactory;

/**
 * 
 * @author kul_grace
 *
 */
@Service
@Qualifier("PTZService")
public class PTZService extends OnvifService {

	@Autowired
	OnvifSoapFactory soapFactory;

	@Autowired
	DeviceService deviceService;

	/**
	 * 
	 * @param onvifDevice
	 * @param profileToken
	 * @param ptzToken
	 * @param xValue
	 * @param yValue
	 * @param zValue
	 * @return
	 */
	public PTZResponse doAbsoluteMove(OnvifDevice onvifDevice) {
		AbsoluteMove absoluteMoveRequest = new AbsoluteMove();
		AbsoluteMoveResponse absoluteMoveResponse = new AbsoluteMoveResponse();
		PTZResponse ptzResponse = new PTZResponse();
		String uri = deviceService.getCapabilities(onvifDevice).getDevice().getXAddr();

		try {
			validatePTZLimit(onvifDevice);
			PTZVector vector = setPTZVector(onvifDevice.getxValue(), onvifDevice.getyValue(), onvifDevice.getzValue());
			absoluteMoveRequest.setProfileToken(onvifDevice.getActiveProfileToken());
			absoluteMoveRequest.setPosition(vector);

			SOAPMessage soapResponse = executeSOAPCall(absoluteMoveRequest, uri, onvifDevice.getUsername(),
					onvifDevice.getPassword(), true);
			absoluteMoveResponse = (AbsoluteMoveResponse) getResponseElement(soapResponse, absoluteMoveResponse);
			ptzResponse.setErrorCode(OnvifConstants.SUCCESS_CODE);
			ptzResponse.setErrorDescription(OnvifConstants.SUCCESS_DESCRIPTION);
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			ptzResponse.setErrorCode("0001");
			ptzResponse.setErrorDescription(e.getMessage());
		} catch (NullPointerException n) {
			ptzResponse.setErrorCode("0002");
			ptzResponse.setErrorDescription(n.getMessage());
		} catch(Exception e) {
			ptzResponse.setErrorCode(OnvifConstants.CATCH_ALL_CODE);
			ptzResponse.setErrorDescription(OnvifConstants.CATCH_ALL_DESC);
		}

		return ptzResponse;

	}

	/**
	 * 
	 * @param onvifDevice
	 * @param profileToken
	 * @param ptzToken
	 * @param xValue
	 * @param yValue
	 * @param zValue
	 * @return
	 */
	public PTZResponse doRelativeMove(OnvifDevice onvifDevice) {
		RelativeMove relativeMoveRequest = new RelativeMove();
		RelativeMoveResponse relativeMoveResponse = new RelativeMoveResponse();
		PTZResponse ptzResponse = new PTZResponse();
		String uri = deviceService.getCapabilities(onvifDevice).getPTZ().getXAddr();
		try {
			validatePTZLimit(onvifDevice);
			PTZVector vector = setPTZVector(onvifDevice.getxValue(), onvifDevice.getyValue(), onvifDevice.getzValue());
			relativeMoveRequest.setProfileToken(onvifDevice.getActiveProfileToken());
			relativeMoveRequest.setTranslation(vector);

			SOAPMessage soapResponse = executeSOAPCall(relativeMoveRequest, uri, onvifDevice.getUsername(),
					onvifDevice.getPassword(), true);
			relativeMoveResponse = (RelativeMoveResponse) getResponseElement(soapResponse, relativeMoveResponse);
			ptzResponse.setErrorCode(OnvifConstants.SUCCESS_CODE);
			ptzResponse.setErrorDescription(OnvifConstants.SUCCESS_DESCRIPTION);
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			ptzResponse.setErrorCode("0001");
			ptzResponse.setErrorDescription(e.getMessage());
		} catch (NullPointerException n) {
			ptzResponse.setErrorCode("0002");
			ptzResponse.setErrorDescription(n.getMessage());
		} catch(Exception e) {
			ptzResponse.setErrorCode(OnvifConstants.CATCH_ALL_CODE);
			ptzResponse.setErrorDescription(OnvifConstants.CATCH_ALL_DESC);
		}

		return ptzResponse;

	}

	/**
	 * 
	 * @param onvifDevice
	 * @return
	 */
	public List<PTZConfiguration> getPTZConfigurations(OnvifDevice onvifDevice) {
		GetConfigurations configRequest = new GetConfigurations();
		GetConfigurationsResponse configResponse = new GetConfigurationsResponse();
		try {
			String uri = deviceService.getCapabilities(onvifDevice).getPTZ().getXAddr();

			SOAPMessage soapResponse = executeSOAPCall(configRequest, uri, onvifDevice.getUsername(),
					onvifDevice.getPassword(), true);

			configResponse = (GetConfigurationsResponse) getResponseElement(soapResponse, configResponse);

			return configResponse.getPTZConfiguration();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		

	}

	
	/**
	 * 
	 * @param onvifDevice
	 * @param ptzToken
	 * @return
	 */
	public GetConfigurationResponse getPTZConfig(OnvifDevice onvifDevice) {
		GetConfiguration configurationRequest = new GetConfiguration();
		GetConfigurationResponse configurationResponse = new GetConfigurationResponse();
		try {
			String uri = deviceService.getCapabilities(onvifDevice).getPTZ().getXAddr();
			configurationRequest.setPTZConfigurationToken(onvifDevice.getActivePtzToken());

			SOAPMessage soapResponse = executeSOAPCall(configurationRequest, uri, onvifDevice.getUsername(),
					onvifDevice.getPassword(), true);
			configurationResponse = (GetConfigurationResponse) getResponseElement(soapResponse,
					configurationResponse);

			return configurationResponse;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	/**
	 * 
	 * @param onvifDevice
	 * @param profileToken
	 * @return
	 */
	public List<PTZPreset> getPresets(OnvifDevice onvifDevice) {
		GetPresets presetsRequest = new GetPresets();
		GetPresetsResponse presetsResponse = new GetPresetsResponse();
		
		try {
			String uri = deviceService.getCapabilities(onvifDevice).getPTZ().getXAddr();
			presetsRequest.setProfileToken(onvifDevice.getActiveProfileToken());

			SOAPMessage soapResponse = executeSOAPCall(presetsRequest, uri, onvifDevice.getUsername(),
					onvifDevice.getPassword(), true);
			presetsResponse = (GetPresetsResponse) getResponseElement(soapResponse, presetsResponse);

			return presetsResponse.getPreset();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	
	/**
	 * 
	 * @param onvifDevice
	 * @param profileToken
	 * @param presetToken
	 * @return
	 */
	public PTZResponse goToPreset(OnvifDevice onvifDevice) {
		GotoPreset presetRequest = new GotoPreset();
		GotoPresetResponse presetResponse = new GotoPresetResponse();
		PTZResponse ptzResponse = new PTZResponse();

		try {
			String uri = deviceService.getCapabilities(onvifDevice).getPTZ().getXAddr();
			presetRequest.setPresetToken(onvifDevice.getPresetToken());
			presetRequest.setProfileToken(onvifDevice.getActiveProfileToken());

			try {
				SOAPMessage soapResponse = executeSOAPCall(presetRequest, uri, onvifDevice.getUsername(),
						onvifDevice.getPassword(), true);
				presetResponse = (GotoPresetResponse) getResponseElement(soapResponse, presetResponse);
				ptzResponse.setErrorCode("0000");
				ptzResponse.setErrorDescription("Success");
			} catch (Exception e) {
				ptzResponse.setErrorCode("9999");
				ptzResponse.setErrorDescription("Catch-All Exception");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return ptzResponse;
	}

	/**
	 * 
	 * @param onvifDevice
	 * @param ptzToken
	 * @param xValue
	 * @param yValue
	 * @param zValue
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 */
	private void validatePTZLimit(OnvifDevice onvifDevice)
			throws IllegalArgumentException, NullPointerException {
		GetConfigurationResponse configResponse = getPTZConfig(onvifDevice);
		if (configResponse != null) {
			PTZConfiguration ptzConfig = configResponse.getPTZConfiguration();
			float xMin = ptzConfig.getPanTiltLimits().getRange().getXRange().getMin();
			float xMax = ptzConfig.getPanTiltLimits().getRange().getXRange().getMax();
			float yMin = ptzConfig.getPanTiltLimits().getRange().getYRange().getMin();
			float yMax = ptzConfig.getPanTiltLimits().getRange().getYRange().getMax();
			float zMin = ptzConfig.getZoomLimits().getRange().getXRange().getMin();
			float zMax = ptzConfig.getZoomLimits().getRange().getXRange().getMax();

			if (onvifDevice.getxValue() < xMin || onvifDevice.getxValue() > xMax) {
				throw new IllegalArgumentException(
						"Bad Value for x. Value should be in between " + xMin + " and " + xMax);
			} else if (onvifDevice.getyValue() < yMin || onvifDevice.getyValue() > yMax) {
				throw new IllegalArgumentException(
						"Bad Value for y, Value should be in between " + yMin + " and " + yMax);
			} else if (onvifDevice.getzValue() < zMin || onvifDevice.getzValue() > zMax) {
				throw new IllegalArgumentException(
						"Bad Value for z. Value should be in between " + zMin + " and " + zMax);
			}
		} else {
			throw new NullPointerException("No Configuration Response");
		}
	}

	/**
	 * 
	 * @param xValue
	 * @param yValue
	 * @param zValue
	 * @return
	 */
	private PTZVector setPTZVector(float xValue, float yValue, float zValue) {
		PTZVector vector = new PTZVector();
		Vector2D panTilt = new Vector2D();
		Vector1D zoom = new Vector1D();

		panTilt.setX(xValue);
		panTilt.setY(yValue);

		zoom.setX(zValue);

		vector.setPanTilt(panTilt);
		vector.setZoom(zoom);

		return vector;
	}

}
