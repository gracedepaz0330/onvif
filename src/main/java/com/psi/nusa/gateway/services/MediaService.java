package com.psi.nusa.gateway.services;

import java.util.List;

import javax.xml.soap.SOAPMessage;

import org.onvif.ver10.media.wsdl.GetProfile;
import org.onvif.ver10.media.wsdl.GetProfileResponse;
import org.onvif.ver10.media.wsdl.GetProfiles;
import org.onvif.ver10.media.wsdl.GetProfilesResponse;
import org.onvif.ver10.media.wsdl.GetStreamUri;
import org.onvif.ver10.media.wsdl.GetStreamUriResponse;
import org.onvif.ver10.schema.MediaUri;
import org.onvif.ver10.schema.Profile;
import org.onvif.ver10.schema.StreamSetup;
import org.onvif.ver10.schema.StreamType;
import org.onvif.ver10.schema.Transport;
import org.onvif.ver10.schema.TransportProtocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.psi.nusa.gateway.constants.OnvifConstants;
import com.psi.nusa.gateway.models.OnvifDevice;

/**
 * 
 * @author kul_grace
 *
 */
@Service
@Qualifier("MediaService")
public class MediaService extends OnvifService {
	
	
	@Autowired
	ConfigurationService configService;
	
	@Autowired
	DeviceService deviceService;
	
	/**
	 * 
	 * @param onvifDevice
	 * @return
	 */
	public List<Profile> getProfiles(OnvifDevice onvifDevice) {
		GetProfiles request = new GetProfiles();
		GetProfilesResponse profiles = new GetProfilesResponse();
		
		try {
			String uri = deviceService.getCapabilities(onvifDevice).getMedia().getXAddr();
			SOAPMessage soapResponse = executeSOAPCall(request, uri, 
					onvifDevice.getUsername(), onvifDevice.getPassword(), true);
			
			profiles = (GetProfilesResponse) getResponseElement(soapResponse, profiles);

			return profiles.getProfiles();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * 
	 * @param onvifDevice
	 * @param profileToken
	 * @return
	 */
	public Profile getProfileByToken(OnvifDevice onvifDevice) {
		GetProfile profileRequest = new GetProfile();
		GetProfileResponse profileResponse = new GetProfileResponse();
		
		try {
			String uri = deviceService.getCapabilities(onvifDevice).getMedia().getXAddr();
			
			profileRequest.setProfileToken(onvifDevice.getActiveProfileToken());
			SOAPMessage soapResponse = executeSOAPCall(profileRequest, uri, 
					onvifDevice.getUsername(), onvifDevice.getPassword(), true);
			
			profileResponse = (GetProfileResponse) getResponseElement(soapResponse, profileResponse);
			
			return profileResponse.getProfile();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * 
	 * @param onvifDevice
	 * @param profileToken
	 * @param protocol
	 * @param streamType
	 * @return
	 */
	public MediaUri getStreamUri(OnvifDevice onvifDevice) {
		GetStreamUri uriRequest = new GetStreamUri();
		GetStreamUriResponse uriResponse = new GetStreamUriResponse();

		try {
			String uri = deviceService.getCapabilities(onvifDevice).getMedia().getXAddr();
			StreamSetup streamSetup = setStreamSetup(onvifDevice.getProtocol(), onvifDevice.getStream());
			uriRequest.setProfileToken(onvifDevice.getActiveProfileToken());
			uriRequest.setStreamSetup(streamSetup);
			SOAPMessage soapResponse = executeSOAPCall(uriRequest, uri, onvifDevice.getUsername(),
					onvifDevice.getPassword(), true);
			uriResponse = (GetStreamUriResponse) getResponseElement(soapResponse, uriResponse);
			if(uriResponse != null) {
				String mediaUri = uriResponse.getMediaUri().getUri();
				if(mediaUri.startsWith(OnvifConstants.RTSP_PROTOCOL)) {
					uriResponse.getMediaUri().setUri(mediaUri.replace(OnvifConstants.RTSP_PROTOCOL, OnvifConstants.RTSP_PROTOCOL + onvifDevice.getUsername() + ":" + onvifDevice.getPassword() + "@"));
				}
			}
			return uriResponse.getMediaUri();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	
	/**
	 * 
	 * @param protocol
	 * @param streamType
	 * @return
	 * @throws IllegalArgumentException
	 */
	private StreamSetup setStreamSetup(String protocol, String streamType) throws IllegalArgumentException {
		StreamSetup streamSetup = new StreamSetup();
		Transport transport = new Transport();
		
		if(protocol.equals(TransportProtocol.RTSP.value())) {
			transport.setProtocol(TransportProtocol.RTSP);
		} else if(protocol.equals(TransportProtocol.HTTP.value())) {
			transport.setProtocol(TransportProtocol.HTTP);
		} else if(protocol.equals(TransportProtocol.UDP.value())) {
			transport.setProtocol(TransportProtocol.UDP);
		} else {
			throw new IllegalArgumentException();
		}
		
		if(streamType.equals(StreamType.RTP_MULTICAST.value())) {
			streamSetup.setStream(StreamType.RTP_MULTICAST);
		} else if(streamType.equals(StreamType.RTP_UNICAST.value())) {
			streamSetup.setStream(StreamType.RTP_UNICAST);
		} else {
			throw new IllegalArgumentException();
		}
		
		streamSetup.setTransport(transport);
		
		return streamSetup;
	}

}
