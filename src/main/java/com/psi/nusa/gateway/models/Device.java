package com.psi.nusa.gateway.models;

public class Device {
    
    private String ip;

    private String port;
    
    private String guid;
    
    private String username;
	
	private String password;
    
    private String driverType;
    
    public Device() {
    	
    }
    
    public Device(String ip, String port, String username, String password, 
    		String driverType, String guid) {
    	this.ip = ip;
    	this.port = port;
    	this.username = username;
    	this.password = password;
    	this.driverType = driverType;
    	this.guid = guid;
    }

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDriverType() {
		return driverType;
	}

	public void setDriverType(String driverType) {
		this.driverType = driverType;
	}

	@Override
	public String toString() {
		return "Device [ip=" + ip + ", port=" + port + ", guid=" + guid + ", username=" + username + ", password="
				+ password + ", driverType=" + driverType + "]";
	}

    
}
