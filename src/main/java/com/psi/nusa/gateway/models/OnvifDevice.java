package com.psi.nusa.gateway.models;

/**
 * 
 * @author kul_grace
 *
 */
public class OnvifDevice extends Device {
	
	public OnvifDevice(String ip, String port, String username, String password, 
    		String driverType, String guid) {
		super(ip, port, username, password, driverType, guid);
	}
	
	public OnvifDevice() {
		super();
	}
	
	private float xValue;
	
	private float yValue;
	
	private float zValue;
	
	private String activeProfileToken;

	private String activePtzToken;
	
	private String presetToken;

	private String protocol;
	
	private String stream;

	public float getxValue() {
		return xValue;
	}

	public void setxValue(float xValue) {
		this.xValue = xValue;
	}

	public float getyValue() {
		return yValue;
	}

	public void setyValue(float yValue) {
		this.yValue = yValue;
	}

	public float getzValue() {
		return zValue;
	}

	public void setzValue(float zValue) {
		this.zValue = zValue;
	}

	public String getPresetToken() {
		return presetToken;
	}

	public void setPresetToken(String presetToken) {
		this.presetToken = presetToken;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public String getActiveProfileToken() {
		return activeProfileToken;
	}

	public void setActiveProfileToken(String activeProfileToken) {
		this.activeProfileToken = activeProfileToken;
	}

	public String getActivePtzToken() {
		return activePtzToken;
	}

	public void setActivePtzToken(String activePtzToken) {
		this.activePtzToken = activePtzToken;
	}

	@Override
	public String toString() {
		return "OnvifDevice [xValue=" + xValue + ", yValue=" + yValue + ", zValue=" + zValue + ", activeProfileToken="
				+ activeProfileToken + ", activePtzToken=" + activePtzToken 
				+ ", presetToken=" + presetToken + ", protocol=" + protocol + ", stream=" + stream + "]";
	}

	
}
