package com.psi.nusa.gateway.models;

import java.util.List;

import org.onvif.ver10.schema.Capabilities;
import org.onvif.ver10.schema.Profile;

public class DeviceConfiguration {
	
	private Device onvifDevice;
	
	private Capabilities capabilities;
	
	private List<Profile> profiles;

	public Capabilities getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(Capabilities capabilities) {
		this.capabilities = capabilities;
	}

	public List<Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public Device getOnvifDevice() {
		return onvifDevice;
	}

	public void setOnvifDevice(Device onvifDevice) {
		this.onvifDevice = onvifDevice;
	}
}
