package com.psi.nusa.gateway.soap;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;

import com.psi.nusa.gateway.utils.SecurityUtil;

/***
 * 
 * @author kul_grace
 * 
 *
 */
@Service
@Qualifier("OnvifSoapFactory")
public class OnvifSoapFactory implements SoapFactory {
	
	@Override
	public SOAPConnection establishSoapConnection() {
		SOAPConnectionFactory soapConnFactory = null;
		SOAPConnection soapConnection = null;
		try {
			soapConnFactory = SOAPConnectionFactory.newInstance();
			soapConnection = soapConnFactory.createConnection();
		} catch (UnsupportedOperationException | SOAPException e) {
			e.printStackTrace();
		}
		
		return soapConnection;
		
	}
	
	@Override
	public void createSoapHeader(SOAPMessage message, String username, String password) {
		
		SoapSecurity soapSecurity = new SoapSecurity(password);
		String encryptedPassword = soapSecurity.getEncryptedPassword();
	
		try {
			if(!StringUtils.isEmpty(encryptedPassword) || !StringUtils.isEmpty(username)) {
				SOAPPart part = message.getSOAPPart();
				SOAPHeader header = message.getSOAPHeader();
				SOAPEnvelope envelop = part.getEnvelope();
				
				envelop.addNamespaceDeclaration("wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
				envelop.addNamespaceDeclaration("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
				
				SOAPElement securityElement = header.addChildElement("Security", "wsse");
				SOAPElement usernameTokenElem = securityElement.addChildElement("UsernameToken", "wsse");

				SOAPElement usernameElem = usernameTokenElem.addChildElement("Username", "wsse");
				usernameElem.setTextContent(username);

				SOAPElement passwordElem = usernameTokenElem.addChildElement("Password", "wsse");
				passwordElem.setAttribute("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest");
				passwordElem.setTextContent(encryptedPassword);

				SOAPElement nonceElem = usernameTokenElem.addChildElement("Nonce", "wsse");
				nonceElem.setAttribute("EncodingType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
				nonceElem.setTextContent(SecurityUtil.encryptNonce(soapSecurity.getNonce()));

				SOAPElement createdElem = usernameTokenElem.addChildElement("Created", "wsu");
				createdElem.setTextContent(soapSecurity.getUtcTime());
			}
		} catch(SOAPException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public SOAPMessage createSOAPMessage(Object requestElement) {
		
		MessageFactory messageFactory;
		try {
			messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
			SOAPMessage soapMessage = messageFactory.createMessage();
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			Marshaller marshaller = JAXBContext.newInstance(requestElement.getClass()).createMarshaller();
			marshaller.marshal(requestElement, document);
			soapMessage.getSOAPBody().addDocument(document);
			
			soapMessage.saveChanges();
			soapMessage.writeTo(System.out);
			return soapMessage;
		} catch (SOAPException | ParserConfigurationException | JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

}
