package com.psi.nusa.gateway.soap;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPMessage;

/***
 * 
 * @author kul_grace
 * 
 * SOAP Client Helper to send SOAP message to SOAP Server
 *
 */
public interface SoapFactory {
	
	/**
	 * Creates SOAP connection
	 * 
	 * @return soap connection
	 */
	SOAPConnection establishSoapConnection();
	
	/**
	 * 
	 * @param message
	 * 			envelop the SOAP request
	 * @param username
	 * 			username request property
	 * @param password
	 * 			password request property
	 */
	void createSoapHeader(SOAPMessage message, String username, String password);
	
	
	/**
	 * 
	 * @param requestElement
	 * 			body of SOAP request
	 * @return soap message
	 */
	SOAPMessage createSOAPMessage(Object requestElement);

}
