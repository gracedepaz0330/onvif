package com.psi.nusa.gateway.soap;

import com.psi.nusa.gateway.utils.DateUtil;
import com.psi.nusa.gateway.utils.SecurityUtil;

public class SoapSecurity {
	
	private String nonce;
	
	private String encryptedPassword;
	
	private String utcTime;
	
	public SoapSecurity() {
		
	}
	
	public SoapSecurity(String password) {
		this.nonce = SecurityUtil.createNonce();
		this.utcTime = DateUtil.getUTCTime();
		this.encryptedPassword = SecurityUtil.encryptPassword(password, nonce, utcTime);
	}

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public String getUtcTime() {
		return utcTime;
	}

	public void setUtcTime(String utcTime) {
		this.utcTime = utcTime;
	}

}
